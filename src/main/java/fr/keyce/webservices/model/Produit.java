/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.keyce.webservices.model;

/**
 *
 * @author cedric
 */
public class Produit {
	
	private Long id;
    private String title;
    private String description;
    private Float prix;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrix() {
		return prix;
	}

	public void setPrix(Float prix) {
		this.prix = prix;
	}
	
	/**
	 * Factory pattern
	 * @param id
	 * @param title
	 * @param description
	 * @param prix
	 * @return 
	 */
	public final static Produit create(Long id, String title, String description, Float prix) {
		Produit p = new Produit();
		p.id = id;
		p.title = title;
		p.description = description;
		p.prix = prix;
		return p;
	}
	
}
