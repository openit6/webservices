/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.keyce.webservices.mock;

import fr.keyce.webservices.model.Produit;
import java.util.Map;
import java.util.TreeMap;

public class Database {

  static Database instance = new Database();

  private Map<Long, Produit> map = new TreeMap<>();

  private Database() {
    Produit p;
    p = Produit.create(1L, "T56", "Canne à pêche 8.8 pieds soie #4/5", 450f);
    map.put(p.getId(), p);
    p = Produit.create(2L, "T42", "Canne à pêche 9.0 pieds soie #4/5", 350f);
    map.put(p.getId(), p);
    p = Produit.create(3L, "T36", "Canne à pêche 9.5 pieds soie #6/7", 250f);
    map.put(p.getId(), p);

  }

  public static Database getInstance() {
    return instance;
  }

  public Map<Long, Produit> getMap() {
    return map;
  }

}