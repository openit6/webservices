package fr.keyce.webservices.service;

import javax.ejb.Local;

import fr.keyce.webservices.dao.ProduitDao;
import fr.keyce.webservices.model.Produit;

@Local
public interface ProduitService extends ProduitDao {

  @Override
  default Produit create(Produit p) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  default void delete(Long id) {
    // TODO Auto-generated method stub
    
  }

  @Override
  default Produit read(Long id) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  default Produit update(Produit p) {
    // TODO Auto-generated method stub
    return null;
  }
  
}
