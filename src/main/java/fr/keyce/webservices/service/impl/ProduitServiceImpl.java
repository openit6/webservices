package fr.keyce.webservices.service.impl;

import javax.ejb.EJB;

import fr.keyce.webservices.dao.ProduitDao;
import fr.keyce.webservices.model.Produit;
import fr.keyce.webservices.service.ProduitService;

public class ProduitServiceImpl implements ProduitService {
  // Injection de dépendance de l'EJB, le serveur application Glassfish
  // Utilise CDI pour init auto le champs
  @EJB
  private ProduitDao dao;

  @Override
  public Produit create(Produit p) {
    return this.dao.create(p);
  }

  @Override
  public void delete(Long id) {
    dao.delete(id);
  }

  @Override
  public Produit read(Long id) {
    return dao.read(id);
  }

  @Override
  public Produit update(Produit p) {
    return this.dao.update(p);
  }

}
