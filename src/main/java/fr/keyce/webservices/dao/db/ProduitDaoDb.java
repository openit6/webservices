package fr.keyce.webservices.dao.db;

import java.util.TreeSet;

import javax.ejb.Stateless;

import fr.keyce.webservices.dao.ProduitDao;
import fr.keyce.webservices.mock.Database;
import fr.keyce.webservices.model.Produit;

@Stateless
public class ProduitDaoDb implements ProduitDao {

  private Database db = Database.getInstance();

  @Override
  public Produit create(Produit p) {
    // Check if produit id is null
    if (p.getId() != null) {
      throw new UnsupportedOperationException("Produit already have an ID");
    }
    // Get last id in map
    TreeSet<Long> treeSet = (TreeSet<Long>) db.getMap().keySet();
    Long lastId = treeSet.last();
    // Auto increment
    lastId += 1;
    p.setId(lastId);
    db.getMap().put(lastId, p);
    return p;
  }

  @Override
  public Produit read(Long id) {
    return this.db.getMap().get(id);
  }

  @Override
  public Produit update(Produit p) {
    if (this.db.getMap().containsValue(p)) {
      this.db.getMap().put(p.getId(), p);
    }
    throw new UnsupportedOperationException("Produit not exists" + p.getId());
  }

  @Override
  public void delete(Long id) {
    this.db.getMap().remove(id);
  }

}
