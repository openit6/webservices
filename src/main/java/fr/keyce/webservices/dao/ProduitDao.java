package fr.keyce.webservices.dao;

import javax.ejb.Local;

import fr.keyce.webservices.model.Produit;

@Local
public interface ProduitDao {
  public Produit create(Produit p);

  public Produit read(Long id);

  public Produit update(Produit p);

  public void delete(Long id);
}
